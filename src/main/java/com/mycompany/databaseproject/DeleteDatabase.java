/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author sairung
 */
public class DeleteDatabase {
    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:coffee.db";
        // Connect //
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        // Delete //
        String sql = "DELETE FROM  product WHERE product_id = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,10);
 
            int status = stmt.executeUpdate();
           
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }

        // Close //
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
