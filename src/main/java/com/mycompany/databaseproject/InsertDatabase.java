/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author sairung
 */
public class InsertDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:coffee.db";
        // Connect //
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        // Insert //
        String sql = "INSERT INTO product(product_id,product_name,product_size,"
                + "product_sw_level,product_price,product_type,product_category) VALUES (?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,11);
            stmt.setString(2, "Candy");
            stmt.setString(3, "-");
            stmt.setString(4, "-");
            stmt.setInt(5,10);
            stmt.setString(6, "-");
            stmt.setInt(7,2);
            int status = stmt.executeUpdate();
           // Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                System.out.println(rs.getInt("product_id") + " " + rs.getString("product_name") + " " + rs.getString("product_size") + " "
                        + rs.getInt("product_sw_level") + " " + rs.getInt("product_price") + " " + rs.getString("product_type") + " " + rs.getInt("product_category"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }

        // Close //
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
